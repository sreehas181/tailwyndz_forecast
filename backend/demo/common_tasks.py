import json

import pandas as pd
from celery.utils.log import get_task_logger
from demo.classification_utils import (
    fine_tune_pycaret_model as c_fine_tune_pycaret_model,
)
from demo.classification_utils import load_model_from_task as c_load_model_from_task
from demo.classification_utils import load_pycaret_config as c_load_pycaret_config
from demo.classification_utils import save_pycaret_config as c_save_pycaret_config
from demo.classification_utils import save_pycaret_model as c_save_pycaret_model
from demo.models import Record
from demo.regression_utils import fine_tune_pycaret_model as r_fine_tune_pycaret_model
from demo.regression_utils import load_model_from_task as r_load_model_from_task
from demo.regression_utils import load_pycaret_config as r_load_pycaret_config
from demo.regression_utils import save_pycaret_config as r_save_pycaret_config
from demo.regression_utils import save_pycaret_model as r_save_pycaret_model
from demo.utils import (
    create_init_record,
    delete_cache,
    get_last_pycaret_result_as_dict,
    get_last_pycaret_result_as_list,
    get_model_plotpath,
    get_or_create_task_path,
    get_record_by_task_id,
    get_workflow_document,
    get_workflow_input_df,
    get_workspace,
    make_arguments,
    parse_arguments,
    save_pull_results,
    set_record_file_if_not_present,
    set_started_status_cache,
    urljoin,
)
from django.core.cache import cache
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from pycaret.classification import compare_models as c_compare_models
from pycaret.classification import create_model as c_create_model
from pycaret.classification import finalize_model as c_finalize_model
from pycaret.classification import load_model as c_load_model
from pycaret.classification import plot_model as c_plot_model
from pycaret.classification import predict_model as c_predict_model
from pycaret.classification import pull as c_pull
from pycaret.classification import setup as c_setup
from pycaret.regression import compare_models as r_compare_models
from pycaret.regression import create_model as r_create_model
from pycaret.regression import finalize_model as r_finalize_model
from pycaret.regression import load_model as r_load_model
from pycaret.regression import plot_model as r_plot_model
from pycaret.regression import predict_model as r_predict_model
from pycaret.regression import pull as r_pull
from pycaret.regression import setup as r_setup
from rest_framework.exceptions import APIException

from backend.celery import app

logger = get_task_logger(__name__)

pycaret_method_map = {
    "classification": {
        "compare_models": c_compare_models,
        "create_model": c_create_model,
        "finalize_model": c_finalize_model,
        "load_model": c_load_model,
        "plot_model": c_plot_model,
        "predict_model": c_predict_model,
        "pull": c_pull,
        "setup": c_setup,
        "fine_tune_pycaret_model": c_fine_tune_pycaret_model,
        "load_model_from_task": c_load_model_from_task,
        "load_pycaret_config": c_load_pycaret_config,
        "save_pycaret_config": c_save_pycaret_config,
        "save_pycaret_model": c_save_pycaret_model,
    },
    "regression": {
        "compare_models": r_compare_models,
        "create_model": r_create_model,
        "finalize_model": r_finalize_model,
        "load_model": r_load_model,
        "plot_model": r_plot_model,
        "predict_model": r_predict_model,
        "pull": r_pull,
        "setup": r_setup,
        "fine_tune_pycaret_model": r_fine_tune_pycaret_model,
        "load_model_from_task": r_load_model_from_task,
        "load_pycaret_config": r_load_pycaret_config,
        "save_pycaret_config": r_save_pycaret_config,
        "save_pycaret_model": r_save_pycaret_model,
    },
}


@app.task(bind=True)
def initialize(self, workspace_type, workspace_id=None, data={}):
    logger.debug("INITIALIZE task started")
    request_id = self.request.id
    workspace = get_workspace(workspace_id)

    filename = data["filename"]
    arguments = data["arguments"]
    compare_arguments = data["compare_arguments"]

    pycaret_result_files = {}
    pull_result_path = get_or_create_task_path(workspace, request_id)

    document, created = get_workflow_document(workspace)
    set_record_file_if_not_present(document, filename, created)
    # NOTE: Might need to look into adding multiple files to the document or update the document file
    record = create_init_record(document, request_id)
    set_started_status_cache(self.request.id)

    df = get_workflow_input_df(workspace, filename)
    logger.debug("INITIALIZE setup started")
    setup = pycaret_method_map[workspace_type]["setup"]
    pull = pycaret_method_map[workspace_type]["pull"]
    _ = setup(df, **make_arguments(arguments))
    logger.debug("INITIALIZE setup finished")
    pycaret_result_files["setup_results"] = save_pull_results(
        pull(), pull_result_path, "setup.csv"
    )

    # metrics_df = get_metrics()
    # metrics_dict = metrics_df.to_dict(orient="records")
    logger.debug("COMPARE_MODELS comparing models started")
    compare_models = pycaret_method_map[workspace_type]["compare_models"]
    compare_models(**parse_arguments(compare_arguments))
    logger.debug("COMPARE_MODELS comparing models finished")

    models_dict = get_last_pycaret_result_as_dict(pull())
    pycaret_result_files["compare_model_results"] = save_pull_results(
        pull(), pull_result_path, "compare_model.csv"
    )

    save_pycaret_config = pycaret_method_map[workspace_type]["save_pycaret_config"]
    setup_pickle_path, model_filename = save_pycaret_config(request_id, workspace)
    logger.debug("INITIALIZE setup config saved")

    create_model_url = urljoin([data["absolute_url"], request_id])

    delete_cache(request_id)
    record.set_file_path(model_filename)
    logger.debug("INITIALIZE task finished")

    return {
        "setup_pickle_path": setup_pickle_path,
        "model_results": models_dict,
        "create_model_url": create_model_url,
        "files": pycaret_result_files,
    }


@app.task(bind=True)
def create_model_task(
    self, workspace_type, workspace_id=None, parent_task_id=None, data={}
):
    logger.debug("CREATE_MODEL started")
    # validations
    provided_model_name = data["model_name"]
    if not provided_model_name:
        raise Exception("Model name is not provided")

    request_id = self.request.id
    workspace = get_workspace(workspace_id)

    pycaret_result_files = {}
    pull_result_path = get_or_create_task_path(workspace, request_id)
    arguments = data["arguments"]

    tune_model_results = {}
    predict_model_results = {}
    set_started_status_cache(request_id)

    # Get record which is part of the parent task
    parent_record = get_record_by_task_id(parent_task_id)
    parent_task_id = str(parent_record.task_id)

    load_pycaret_config = pycaret_method_map[workspace_type]["load_pycaret_config"]
    load_pycaret_config(parent_task_id, workspace)
    logger.debug("CREATE_MODEL config loaded")

    logger.debug("CREATE_MODEL creating model started")
    create_model = pycaret_method_map[workspace_type]["create_model"]
    model = create_model(provided_model_name, **parse_arguments(arguments))

    pull = pycaret_method_map[workspace_type]["pull"]
    pycaret_result_files["create_model_results"] = save_pull_results(
        pull(), pull_result_path, "create_model.csv"
    )

    model_results = get_last_pycaret_result_as_list(pull())
    if "fine_tune" in data and data["fine_tune"]:
        logger.debug("CREATE_MODEL fine tune started")
        fine_tune_pycaret_model = pycaret_method_map[workspace_type][
            "fine_tune_pycaret_model"
        ]
        fine_tune_pycaret_model(model)
        logger.debug("CREATE_MODEL fine tune finished")
        pycaret_result_files["fine_tune_results"] = save_pull_results(
            pull(), pull_result_path, "fine_tune.csv"
        )
        tune_model_results = get_last_pycaret_result_as_list(pull())

    model_filename = "model"
    save_pycaret_model = pycaret_method_map[workspace_type]["save_pycaret_model"]
    model_pickle_path = save_pycaret_model(model, request_id, workspace, model_filename)
    logger.debug("CREATE_MODEL model saved")

    # Create new record
    record = Record.objects.create(
        document=parent_record.document,
        task_id=request_id,
        parent_record=parent_record,
        stage="CREATE_MODEL",
    )
    record.set_file_path(model_filename)

    save_pycaret_config = pycaret_method_map[workspace_type]["save_pycaret_config"]
    save_pycaret_config(request_id, workspace)
    logger.debug("CREATE_MODEL setup config saved")

    if "predict_model" in data and data["predict_model"]:
        logger.debug("CREATE_MODEL predict model started")
        predict_model = pycaret_method_map[workspace_type]["predict_model"]
        predict_model(model)
        logger.debug("CREATE_MODEL predict model finished")
        predict_model_results = get_last_pycaret_result_as_list(pull())
        pycaret_result_files["predict_model_results"] = save_pull_results(
            pull(), pull_result_path, "predict_model.csv"
        )

    deploy_model_url = urljoin([data["absolute_url"], request_id])
    analyze_model_url = urljoin([data["analyze_model_absolute_url"], request_id])

    logger.debug("CREATE_MODEL task finished")
    return {
        "model_pickle_path": model_pickle_path,
        "model_results": model_results,
        "tune_model_results": tune_model_results,
        "predict_model_results": predict_model_results,
        "deploy_model_url": deploy_model_url,
        "analyze_model_url": analyze_model_url,
        "files": pycaret_result_files,
    }


@app.task(bind=True)
def analyze_model(
    self, workspace_type, workspace_id=None, parent_task_id=None, data={}
):
    logger.debug("ANALYZE_MODEL started")
    request_id = self.request.id
    workspace = get_workspace(workspace_id)
    set_started_status_cache(request_id)

    arguments = data["plot_arguments"]
    # Get record which is part of the parent task
    create_model_task = get_record_by_task_id(parent_task_id)
    create_model_task_id = str(create_model_task.task_id)

    load_pycaret_config = pycaret_method_map[workspace_type]["load_pycaret_config"]
    load_pycaret_config(create_model_task_id, workspace)
    logger.debug("ANALYZE_MODEL setup config loaded")

    logger.debug("ANALYZE_MODEL loading model started")
    load_model_from_task = pycaret_method_map[workspace_type]["load_model_from_task"]
    model = load_model_from_task(create_model_task)
    logger.debug("ANALYZE_MODEL loading model finished")

    logger.debug("ANALYZE_MODEL plot model started")

    plot_path = get_or_create_task_path(workspace, request_id)
    plot_model = pycaret_method_map[workspace_type]["plot_model"]
    plot = plot_model(model, **parse_arguments(arguments), save=plot_path)
    # plot_full_path = f"{plot_path}/{plot}"

    logger.info(f"ANALYZE_MODEL {plot} ----------")
    logger.debug("ANALYZE_MODEL plot model finished")

    # Create new record
    record = Record.objects.create(
        document=create_model_task.document,
        task_id=request_id,
        parent_record=create_model_task,
        stage="ANALYZE_MODEL",
    )
    record.file.name = plot
    record.save(update_fields=["file"])
    plot_file_path = record.file.path
    plot_file_url = record.file.url
    logger.debug("ANALYZE_MODEL saved")

    logger.debug("ANALYZE_MODEL task finished")
    return {
        "plot_file_path": plot_file_path,
        "files": {
            "plot_file_url": plot_file_url,
        },
        "deploy_url": urljoin([data["absolute_url"], request_id]),
    }


@app.task(bind=True)
def deploy_model(self, workspace_type, workspace_id=None, parent_task_id=None, data={}):
    logger.debug("DEPLOY_MODEL started")
    request_id = self.request.id
    workspace = get_workspace(workspace_id)
    set_started_status_cache(request_id)

    pycaret_result_files = {}
    pull_result_path = get_or_create_task_path(workspace, request_id)

    # Get record which is part of the parent task
    create_model_task = get_record_by_task_id(parent_task_id)
    create_model_task_id = str(create_model_task.task_id)

    load_pycaret_config = pycaret_method_map[workspace_type]["load_pycaret_config"]
    load_pycaret_config(create_model_task_id, workspace)
    logger.debug("DEPLOY_MODEL setup config loaded")

    logger.debug("DEPLOY_MODEL loading model started")
    load_model_from_task = pycaret_method_map[workspace_type]["load_model_from_task"]
    model = load_model_from_task(create_model_task)
    logger.debug("DEPLOY_MODEL loading model finished")

    logger.debug("DEPLOY_MODEL finalizing model started")
    finalize_model = pycaret_method_map[workspace_type]["finalize_model"]
    final_model = finalize_model(model)
    logger.debug("DEPLOY_MODEL finalizing model finished")

    pull = pycaret_method_map[workspace_type]["pull"]
    pycaret_result_files["finalize_model_results"] = save_pull_results(
        pull(), pull_result_path, "finalize_model.csv"
    )

    # final_model_results_df = pull()
    # final_model_results = final_model_results_df.to_dict(orient="records")

    model_filename = "model"
    save_pycaret_model = pycaret_method_map[workspace_type]["save_pycaret_model"]
    save_pycaret_model(final_model, request_id, workspace, model_filename)
    logger.debug("DEPLOY_MODEL model saved")

    # Create new record
    record = Record.objects.create(
        document=create_model_task.document,
        task_id=request_id,
        parent_record=create_model_task_id,
        stage="DEPLOY_MODEL",
    )
    record.set_file_path(model_filename)
    model_pickle_path = record.file.path
    logger.debug("DEPLOY_MODEL saved")

    logger.debug("DEPLOY_MODEL task finished")
    return {
        "model_pickle_path": model_pickle_path,
        # "final_model_results": final_model_results,
        "predict_url": urljoin([data["absolute_url"], request_id]),
    }


@app.task(bind=True)
def predict(self, workspace_type, record_id=None, input_data={}):
    print(record_id, input_data)
    record = get_record_by_task_id(record_id)
    setup_id = str(record.parent_record.task_id)

    load_pycaret_config = pycaret_method_map[workspace_type]["load_pycaret_config"]
    load_pycaret_config(setup_id, record.document.workspace)
    logger.debug("PREDICT_MODEL config loaded")

    model_path = record.file.path
    load_model = pycaret_method_map[workspace_type]["load_model"]
    model = load_model(model_path)
    logger.debug("PREDICT_MODEL loaded")

    input_data = json.loads(input_data)
    input_df = pd.DataFrame([input_data])

    try:
        logger.debug("PREDICT_MODEL predicting started")
        predict_model = pycaret_method_map[workspace_type]["predict_model"]
        predictions = get_last_pycaret_result_as_list(
            predict_model(model, data=input_df)
        )
        logger.debug("PREDICT_MODEL prediction finished")
        return {"predictions": predictions}
    except Exception as e:
        raise APIException(
            {
                "error": str(e),
            }
        )
