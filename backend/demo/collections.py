from bson.json_util import dumps
from django.conf import settings
from matplotlib import projections
from pymongo import MongoClient


class MongoConnection:
    def __init__(self, backend, db, collection) -> None:
        self.client = MongoClient(backend)
        self.db = self.client[db]
        self.collection = self.db[collection]


class CeleryTaskResults(MongoConnection):
    def __init__(self) -> None:
        result_backend = settings.CELERY_RESULT_BACKEND
        database_collection_config = settings.CELERY_MONGODB_BACKEND_SETTINGS
        MongoConnection.__init__(
            self,
            result_backend,
            database_collection_config["database"],
            database_collection_config["taskmeta_collection"],
        )

    def get_task_result(self, task_id):
        return self.collection.find_one({"_id": task_id})

    def get_all_task_results(self):
        projection = {
            "result": 0,
            "traceback": 0,
            "children": 0,
        }
        response = list(
            self.collection.find(projection=projection).sort("date_done", -1)
        )
        # response = dumps(response)
        return response
