import json
import os

from demo.utils import document_upload_to, get_workspace_path, record_upload_to
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext as _


# Create your models here.
class Workspace(models.Model):
    WORKSPACE_TYPES = (
        ("CLASSIFICATION", "Classification"),
        ("REGRESSION", "Regression"),
        ("TIMESERIES", "Timeseries"),
    )
    name = models.CharField(max_length=256)
    type = models.CharField(
        max_length=256, choices=WORKSPACE_TYPES, default="CLASSIFICATION"
    )

    class Meta:
        verbose_name = _("workspace")
        verbose_name_plural = _("workspaces")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("_detail", kwargs={"pk": self.pk})

    def save(self, *args, **kwargs) -> None:
        path = get_workspace_path(self.name)
        # TODO: Prevent same name for workspace
        os.makedirs(path, exist_ok=True)
        return super().save(*args, **kwargs)


class Document(models.Model):

    workspace = models.ForeignKey(Workspace, on_delete=models.CASCADE)
    file = models.FileField(upload_to=document_upload_to)

    class Meta:
        verbose_name = _("document")
        verbose_name_plural = _("documents")

    def filename(self):
        if self.file:
            return os.path.basename(self.file.name)
        return None

    def set_file_path(self, filename):
        workspace_name = self.workspace.name
        self.file.name = f"datasets/{workspace_name}/{filename}"
        self.save(update_fields=["file"])


class Record(models.Model):
    document = models.ForeignKey(
        Document, on_delete=models.CASCADE, null=True, blank=True
    )
    file = models.FileField(upload_to=record_upload_to, null=True, blank=True)
    task_id = models.UUIDField(blank=False, null=False)
    stage = models.CharField(max_length=256)
    details = models.TextField(blank=True, null=True)
    parent_record = models.ForeignKey(
        "Record", on_delete=models.CASCADE, null=True, blank=True
    )
    created_at = models.DateTimeField(auto_now_add=True)
    uploaded_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _("record")
        verbose_name_plural = _("records")

    def __str__(self):
        return f"{self.document.workspace.name} - {self.task_id}"

    def get_absolute_url(self):
        return reverse("_detail", kwargs={"pk": self.pk})

    def get_details(self):
        if self.details:
            return json.loads(self.details)
        return {}

    def update_details(self, update_details):
        details = self.get_details()
        details.update(update_details)
        self.details = json.dumps(details)
        self.save(update_fields=["details"])

    def filename(self):
        if self.file:
            return os.path.basename(self.file.name)
        return None

    def set_file_path(self, filename):
        workspace_name = self.document.workspace.name
        self.file.name = f"datasets/{workspace_name}/{self.task_id}/{filename}"
        self.save(update_fields=["file"])
