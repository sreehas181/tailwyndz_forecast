from .common_tasks import analyze_model as common_analyze_model
from .common_tasks import create_model_task as common_create_model_task
from .common_tasks import deploy_model as common_deploy_model
from .common_tasks import initialize as common_initialize
from .common_tasks import predict as common_predict


def initialize(workspace_id=None, data={}):
    task = common_initialize.delay("classification", workspace_id, data)
    return task


def create_model_task(workspace_id=None, parent_task_id=None, data={}):
    task = common_create_model_task.delay(
        "classification", workspace_id, parent_task_id, data
    )
    return task


def analyze_model(workspace_id=None, parent_task_id=None, data={}):
    task = common_analyze_model.delay(
        "classification", workspace_id, parent_task_id, data
    )
    return task


def deploy_model(workspace_id=None, parent_task_id=None, data={}):
    task = common_deploy_model.delay(
        "classification", workspace_id, parent_task_id, data
    )
    return task


def predict(record_id=None, input_data={}):
    task = common_predict.delay("classification", record_id, input_data)
    return task
