from rest_framework import routers

from .views import TaskResultViewSet, WorkspaceViewSet

router = routers.DefaultRouter()

router.register(r"workspace", WorkspaceViewSet, basename="workspace")
router.register(r"task", TaskResultViewSet, basename="task")

urlpatterns = [] + router.urls
