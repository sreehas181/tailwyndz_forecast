from bson import ObjectId
from bson.errors import InvalidId
from django.utils.encoding import smart_text
from rest_framework.exceptions import ValidationError
from rest_framework.fields import BooleanField, CharField, JSONField
from rest_framework.serializers import (
    Field,
    HyperlinkedIdentityField,
    ModelSerializer,
    Serializer,
)

from .models import Document, Record, Workspace


class NoneSerializer(Serializer):
    def create(self, validated_data):
        pass


class WorkspaceSerializer(ModelSerializer):
    url = HyperlinkedIdentityField(view_name="workspace-detail", read_only=True)

    class Meta:
        model = Workspace
        fields = ("id", "name", "url", "type")


class WorkspaceDetailSerializer(ModelSerializer):
    class Meta:
        model = Workspace
        fields = ("id", "name")


class DocumentModelSerializer(ModelSerializer):
    class Meta:
        model = Document
        fields = "__all__"


class SetupSerializer(Serializer):
    filename = CharField()
    arguments = JSONField()
    compare_arguments = JSONField()

    def validate(self, attrs):
        if not attrs["filename"]:
            raise ValidationError("No filename provided")
        return super().validate(attrs)


class CreateModelSerializer(Serializer):
    model_name = CharField()
    arguments = JSONField()
    fine_tune = BooleanField()
    predict_model = BooleanField()

    def validate(self, attrs):
        if not attrs["model_name"]:
            raise ValidationError("No model name provided")
        return super().validate(attrs)


class RecordSerializer(ModelSerializer):
    class Meta:
        model = Record
        fields = "__all__"


class DeployModelSerializer(Serializer):
    arguments = JSONField()


class AnalyzeModelSerializer(Serializer):
    plot_arguments = JSONField()


class PredictSerializer(Serializer):
    input_data = JSONField()

    def validate(self, attrs):
        if not attrs["input_data"]:
            raise ValidationError("No input data provided")
        return super().validate(attrs)
