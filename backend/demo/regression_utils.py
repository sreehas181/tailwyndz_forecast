import os

from pycaret.regression import (
    load_config,
    load_model,
    save_config,
    save_model,
    tune_model,
)

from .utils import get_or_create_task_path, get_task_setup_folder_path


def save_pycaret_config(task_id, workspace):
    setup_pickle_path = get_or_create_task_path(workspace, task_id)
    model_filename = f"{task_id}.pkl"
    setup_pickle_path += f"/{model_filename}"
    save_config(setup_pickle_path)
    return setup_pickle_path, model_filename


def save_pycaret_model(model, task_id, workspace, model_filename):
    model_pickle_path = get_or_create_task_path(workspace, task_id)
    # model_filename = f"{request_id}"
    model_pickle_path += f"/{model_filename}"
    save_model(model, model_pickle_path, model_only=True)
    return model_pickle_path


def load_pycaret_config(task_id, workspace):
    task_folder = get_task_setup_folder_path(workspace.name, task_id)
    setup_pickle_path = os.path.join(task_folder, f"{task_id}.pkl")
    load_config(setup_pickle_path)


def fine_tune_pycaret_model(model):
    model = tune_model(model)


def load_model_from_task(task):
    model_path = task.file.path
    model = load_model(model_path)
    return model
