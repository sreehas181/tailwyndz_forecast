import json
from datetime import datetime

from demo.collections import CeleryTaskResults
from demo.models import Document, Record, Workspace
from demo.serializers import (
    AnalyzeModelSerializer,
    CreateModelSerializer,
    DeployModelSerializer,
    DocumentModelSerializer,
    PredictSerializer,
    RecordSerializer,
    SetupSerializer,
    WorkspaceDetailSerializer,
    WorkspaceSerializer,
)
from demo.tasks import (
    analyze_model_task,
    create_model,
    deploy_model,
    initialize,
    predict,
)
from demo.utils import (
    cache_task_progress,
    get_tasks_from_cache,
    get_wokspace_files,
    get_workspace_file_path,
)
from django.core.cache import cache
from django.urls import reverse
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.status import HTTP_406_NOT_ACCEPTABLE
from rest_framework.viewsets import ModelViewSet, ViewSet


def run_task_return_task(task, *args, **kwargs):
    task_details = task(**kwargs)
    task = task_details["task"]
    return task


def make_url(request, url_name, *args):
    rurl = reverse(url_name, args=args)
    url = request.build_absolute_uri(rurl)
    return url


def make_task_url(task, request):
    # task_url = f"{request.build_absolute_uri()}{task.id}"
    task_url = make_url(request, "task-detail", task.id)
    return task_url


class WorkspaceViewSet(ModelViewSet):
    queryset = Workspace.objects.all()
    serializer_class = WorkspaceSerializer

    def get_serializer_class(self):
        if self.action == "retrieve":
            return WorkspaceDetailSerializer
        return super().get_serializer_class()

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        workspace = self.get_object()
        files = get_wokspace_files(workspace)
        tasks = get_tasks_from_cache(workspace, files)
        response.data["tasks"] = tasks
        return response

    @action(
        detail=True,
        methods=["get", "post"],
        permission_classes=[AllowAny],
        serializer_class=DocumentModelSerializer,
        url_path="upload",
    )
    def upload(self, request, pk=None):
        workspace = self.get_object()
        if request.method == "POST":
            serializer = self.serializer_class(data=request.data)
            serializer.is_valid(raise_exception=True)
            upload_file = serializer.validated_data["file"]
            Document.objects.create(workspace=workspace, file=upload_file)
            return Response(serializer.data)
        documents = Document.objects.filter(workspace=workspace)
        serializer = self.serializer_class(documents, many=True)
        return Response(serializer.data)

    @action(
        detail=True,
        methods=["get"],
        permission_classes=[AllowAny],
        serializer_class=DocumentModelSerializer,
        url_path="upload/(?P<record_id>[0-9]+)",
    )
    def update_detail(self, request, pk=None, record_id=None):
        record = Document.objects.get(pk=record_id)
        serializer = self.serializer_class(record)
        return Response(serializer.data)

    @action(
        detail=True,
        methods=["get", "post"],
        permission_classes=[AllowAny],
        serializer_class=SetupSerializer,
        url_path="initialize-and-train",
        name="1. Setup and train",
    )
    def initialize_and_train(self, request, pk=None):
        workspace = self.get_object()
        workspace_name = workspace.name
        if request.method == "POST":
            data = request.data.dict()
            filename = data["filename"]
            absolute_file_path = get_workspace_file_path(workspace_name, filename)
            if not absolute_file_path:
                raise ValidationError

            if data["arguments"] == "null":
                data["arguments"] = {}

            if data["compare_arguments"] == "null":
                data["compare_arguments"] = {}

            data["absolute_url"] = request.build_absolute_uri(
                reverse("workspace-create-model", args=[workspace.id])
            )

            init_kwargs = {
                "task": initialize,
                "workspace_id": workspace.id,
                "data": data,
                "workspace_type": workspace.type,
            }
            task = run_task_return_task(**init_kwargs)
            task_url = make_task_url(task, request)
            cache_task_progress(
                workspace, filename, task.id, "INITIALIZE_TRAIN", task_url
            )
            return Response({"status": "started", "task_url": task_url})

        # write the code to get the media files from the path
        files = get_wokspace_files(workspace)
        return Response(
            {"files": files},
        )

    @action(
        detail=True,
        methods=["get"],
        permission_classes=[AllowAny],
        serializer_class=CreateModelSerializer,
        url_path="create-model",
        name="2. Create model",
    )
    def create_model(self, request, pk=None):
        """This api does the train, optimize and analyze of the model"""
        workspace = self.get_object()
        document = Document.objects.get(workspace=workspace)
        init_tasks = document.record_set.filter(stage="INITIALIZE_TRAIN").order_by(
            "-id"
        )
        create_model_url_list = []
        for task in init_tasks:
            create_model_url_list.append(
                {
                    "id": task.id,
                    "create_model_api": request.build_absolute_uri(
                        reverse(
                            "workspace-create-model-detail",
                            args=[workspace.id, str(task.task_id)],
                        )
                    ),
                    "created_at": str(task.created_at),
                }
            )
        return Response(create_model_url_list)

    @action(
        detail=True,
        methods=["get", "post"],
        permission_classes=[AllowAny],
        serializer_class=CreateModelSerializer,
        url_path="create-model/(?P<task_id>[^/.]+)",
    )
    def create_model_detail(self, request, pk=None, task_id=None):
        """This api does the train, optimize and analyze of the model"""
        workspace = self.get_object()
        document = Document.objects.get(workspace=workspace)
        record = document.record_set.get(task_id=task_id)
        if request.method == "POST":
            data = request.data.dict()

            if data["arguments"] == "null":
                data["arguments"] = {}

            data["absolute_url"] = make_url(
                request, "workspace-deploy-model", workspace.id
            )
            data["analyze_model_absolute_url"] = request.build_absolute_uri(
                reverse("workspace-analyze-model", args=[workspace.id])
            )

            create_model_kwargs = {
                "task": create_model,
                "workspace_id": workspace.id,
                "parent_task_id": task_id,
                "data": data,
                "workspace_type": workspace.type,
            }
            task = run_task_return_task(**create_model_kwargs)
            task_url = make_task_url(task, request)

            filename = record.filename()
            cache_task_progress(workspace, filename, task.id, "CREATE_MODEL", task_url)
            return Response({"status": "started", "task_url": task_url})
        record_dict = RecordSerializer(instance=record).data
        return Response(record_dict)

    @action(
        detail=True,
        methods=["get"],
        permission_classes=[AllowAny],
        serializer_class=AnalyzeModelSerializer,
        url_path="analyze-model",
        name="3. Analyze model",
    )
    def analyze_model(self, request, pk=None):
        """This api does the analyzement of the model"""
        workspace = self.get_object()
        document = Document.objects.get(workspace=workspace)
        init_tasks = document.record_set.filter(stage="ANALYZE_MODEL").order_by("-id")
        analyze_model_url_list = []
        for task in init_tasks:
            analyze_model_url_list.append(
                {
                    "id": task.id,
                    "analyze_model_api": request.build_absolute_uri(
                        reverse(
                            "workspace-analyze-model-detail",
                            args=[workspace.id, str(task.task_id)],
                        )
                    ),
                    "created_at": str(task.created_at),
                }
            )
        return Response(analyze_model_url_list)

    @action(
        detail=True,
        methods=["get", "post"],
        permission_classes=[AllowAny],
        serializer_class=AnalyzeModelSerializer,
        url_path="analyze-model/(?P<task_id>[^/.]+)",
    )
    def analyze_model_detail(self, request, pk=None, task_id=None):
        """This api does the train, optimize and analyze of the model"""
        workspace = self.get_object()
        document = Document.objects.get(workspace=workspace)
        record = document.record_set.get(task_id=task_id)
        if request.method == "POST":
            data = request.data.dict()

            if data["plot_arguments"] == "null":
                data["plot_arguments"] = {}

            data["absolute_url"] = make_url(
                request, "workspace-deploy-model", workspace.id
            )

            # task_details = analyze_model.apply_async(args=[workspace.id, task_id, data, workspace.type])
            # task = task_details['task']
            # task_url = request.build_absolute_uri(
            #     reverse("task-detail", args=[task.id])
            # )

            analyze_model_kwargs = {
                "task": analyze_model_task,
                "workspace_id": workspace.id,
                "parent_task_id": task_id,
                "data": data,
                "workspace_type": workspace.type,
            }
            task = run_task_return_task(**analyze_model_kwargs)
            task_url = make_task_url(task, request)

            filename = record.filename()
            cache_task_progress(workspace, filename, task.id, "ANALYZE_MODEL", task_url)
            return Response({"status": "started", "task_url": task_url})
        record_dict = RecordSerializer(instance=record).data
        return Response(record_dict)

    @action(
        detail=True,
        methods=["get"],
        permission_classes=[AllowAny],
        serializer_class=DeployModelSerializer,
        url_path="deploy-model",
        name="4. Deploy model",
    )
    def deploy_model(self, request, pk=None, task_id=None):
        """This api does the deployment of the model"""
        workspace = self.get_object()
        document = Document.objects.get(workspace=workspace)
        init_tasks = document.record_set.filter(stage="CREATE_MODEL").order_by("-id")
        deploy_model_url_list = []
        for task in init_tasks:
            deploy_model_url_list.append(
                {
                    "id": task.id,
                    "deploy_model_api": request.build_absolute_uri(
                        reverse(
                            "workspace-deploy-model-detail",
                            args=[workspace.id, str(task.task_id)],
                        )
                    ),
                    "created_at": str(task.created_at),
                }
            )
        return Response(deploy_model_url_list)

    @action(
        detail=True,
        methods=["get", "post"],
        permission_classes=[AllowAny],
        serializer_class=DeployModelSerializer,
        url_path="deploy-model/(?P<task_id>[^/.]+)",
    )
    def deploy_model_detail(self, request, pk=None, task_id=None):
        """This api does the train, optimize and analyze of the model"""
        workspace = self.get_object()
        document = Document.objects.get(workspace=workspace)
        record = document.record_set.get(task_id=task_id)
        if request.method == "POST":
            data = request.data.dict()

            if data["arguments"] == "null":
                data["arguments"] = {}

            data["absolute_url"] = make_url(request, "workspace-predict", workspace.id)

            # task_details = deploy_model.apply_async(args=[workspace.id, task_id, data, workspace.type])
            # task = task_details['task']
            # task_url = request.build_absolute_uri(
            #     reverse("task-detail", args=[task.id])
            # )

            deploy_model_kwargs = {
                "task": deploy_model,
                "workspace_id": workspace.id,
                "parent_task_id": task_id,
                "data": data,
                "workspace_type": workspace.type,
            }
            task = run_task_return_task(**deploy_model_kwargs)
            task_url = make_task_url(task, request)

            filename = record.filename()
            cache_task_progress(workspace, filename, task.id, "DEPLOY_MODEL", task_url)
            return Response({"status": "started", "task_url": task_url})
        record_dict = RecordSerializer(instance=record).data
        return Response(record_dict)

    @action(
        detail=True,
        methods=["get", "post"],
        permission_classes=[AllowAny],
        serializer_class=SetupSerializer,
        url_path="store_engineered_features",
    )
    def store_engineered_features(self, request, pk=None):
        task_results = {}
        return Response(
            {"status": "Not implemented", "pk": pk, "task_results": task_results}
        )

    @action(
        detail=True,
        methods=["get"],
        permission_classes=[AllowAny],
        serializer_class=PredictSerializer,
        url_path="predict",
        name="5. Predict",
    )
    def predict(self, request, pk=None):
        workspace = self.get_object()
        document = Document.objects.get(workspace=workspace)
        init_tasks = document.record_set.filter(stage="DEPLOY_MODEL").order_by("-id")
        predict_url_list = []
        for task in init_tasks:
            task_id = str(task.task_id)
            task_result = CeleryTaskResults().get_task_result(task_id)
            if type(task_result) == dict:
                if "status" in task_result and task_result["status"] == "SUCCESS":
                    predict_url_list.append(
                        {
                            "id": task.id,
                            "result": json.loads(task_result["result"]),
                            "created_at": str(task.created_at),
                        }
                    )
                continue
            for result in task_result:
                if "status" in result and result["status"] == "SUCCESS":
                    predict_url_list.append(
                        {
                            "id": task.id,
                            "result": json.loads(result["result"]),
                            "created_at": str(task.created_at),
                        }
                    )
        return Response(predict_url_list)

    @action(
        detail=True,
        methods=["get", "post"],
        permission_classes=[AllowAny],
        serializer_class=PredictSerializer,
        url_path="predict/(?P<task_id>[^/.]+)",
    )
    def predict_detail(self, request, pk=None, task_id=None):
        workspace = self.get_object()
        if request.method == "POST":
            data = request.data
            input_data = data["input_data"]
            predictions = predict(task_id, input_data, workspace.type)
            return Response(predictions["task"]["predictions"])
        return Response(
            {"status": "Please do send a POST request"}, HTTP_406_NOT_ACCEPTABLE
        )


class TaskResultViewSet(ViewSet):
    def list(self, request):
        TaskResults = CeleryTaskResults()
        task_results = TaskResults.get_all_task_results()
        for task in task_results:
            task["task_url"] = request.build_absolute_uri(
                reverse("task-detail", args=[task["_id"]])
            )
        return Response(task_results)

    def retrieve(self, request, pk=None):
        TaskResult = CeleryTaskResults()
        task_result = TaskResult.get_task_result(pk)
        if task_result:
            task_result["result"] = json.loads(task_result["result"])
            return Response(task_result)
        cache_hit = cache.get(pk)
        if cache_hit:
            return Response(cache_hit)
        return Response("Task not found")
