import json
import os
from datetime import datetime
from os import listdir
from os.path import isfile, join
from uuid import uuid4

import pandas as pd
from celery.utils.log import get_task_logger
from django.conf import settings
from django.core.cache import cache

logger = get_task_logger(__name__)


def get_workspace_path(workspace_name):
    media_path = settings.MEDIA_ROOT
    path = join(media_path, "datasets", workspace_name)
    return path


def get_task_setup_folder_path(workspace_name, task_id):
    media_path = settings.MEDIA_ROOT
    path = join(media_path, "datasets", workspace_name, task_id)
    return path


def get_wokspace_files(workspace):
    workspace_name = workspace.name
    workspace_datatset_path = get_workspace_path(workspace_name)
    myfiles = [
        file
        for file in listdir(workspace_datatset_path)
        if isfile(join(workspace_datatset_path, file))
    ]
    return myfiles


def get_workspace_list():
    media_path = settings.MEDIA_ROOT
    path = join(media_path, "datasets")
    myfiles = [file for file in listdir(path) if isfile(join(path, file))]
    return myfiles


def get_workspace_file_path(workspace_name, file_name):
    workspace_datatset_path = get_workspace_path(workspace_name)
    file_path = join(workspace_datatset_path, file_name)
    return file_path


def parse_arguments(arguments):
    if type(arguments) is not dict:
        arguments = json.loads(arguments)
    return arguments


def make_arguments(arguments):
    arguments = parse_arguments(arguments)
    target = "default"
    if "target" in arguments and arguments["target"]:
        target = arguments["target"]
    logger.info(arguments)
    arguments["target"] = target
    # arguments["fix_imbalance"] = True
    arguments["profile"] = False
    arguments["silent"] = True
    arguments["html"] = False
    return arguments


def cache_task_progress(workspace, file_name, task_id, task_name, task_url):
    cache_hit = cache.get(f"{workspace.id}:{task_name}")
    result = {
        "id": task_id,
        "stage": task_name,
        "url": task_url,
        "started_at": datetime.now(),
        "filename": file_name,
    }
    if cache_hit:
        cache_hit["tasks"].insert(0, result)
        cache.set(f"{workspace.id}:{task_name}", cache_hit, timeout=None)
    else:
        cache.set(f"{workspace.id}:{task_name}", {"tasks": [result]}, timeout=None)
    logger.debug("cache_task_progress", result)


def get_task_details_from_cache(workspace, file_name):
    cache_hit = cache.get(f"{workspace.id}:{file_name}")
    logger.debug("get_task_details_from_cache", cache_hit)
    return cache_hit


def get_tasks_from_cache(workspace, files):
    tasks = {}
    for filename in files:
        tasks[filename] = get_task_details_from_cache(workspace, filename)
    return tasks


def set_started_status_cache(request_id):
    cache_value = {
        "_id": request_id,
        "status": "STARTED",
        "result": "",
        "traceback": None,
        "children": [],
        "date_done": datetime.now(),
    }
    cache.set(request_id, cache_value, timeout=None)


def delete_cache(request_id):
    cache.delete(request_id)


def document_upload_to(instance, filename):
    return get_workspace_file_path(instance.workspace.name, filename)


def record_upload_to(instance, filename):
    filename = f"{instance.task_id}/{filename}"
    return get_workspace_file_path(instance.workspace.name, filename)


def urljoin(pieces):
    return "/".join(s.strip("/") for s in pieces)


def get_workspace(workspace_id):
    from demo.models import Workspace

    workspace = Workspace.objects.get(id=workspace_id)
    return workspace


def create_init_record(document, task_id, stage="INITIALIZE_TRAIN"):
    from demo.models import Record

    record = Record.objects.create(document=document, task_id=task_id, stage=stage)
    return record


def get_record_by_task_id(task_id):
    from demo.models import Record

    record = Record.objects.get(task_id=task_id)
    return record


def get_record_by_parent_task_id(task_id):
    from demo.models import Record

    record = Record.objects.get(parent_task__id=task_id)
    return record


def get_document_from_parent_record(task_id):
    record = get_record_by_task_id(task_id)
    document = record.document
    return document


def get_workflow_document(workspace):
    from demo.models import Document

    document, created = Document.objects.get_or_create(workspace=workspace)
    return document, created


def set_record_file_if_not_present(document, filename, created=False):
    if created:
        if not document.filename():
            document.set_file_path(filename)
            document.save()


def get_workflow_input_df(workspace, filename):
    absolute_file_path = get_workspace_file_path(workspace.name, filename)
    df = pd.read_csv(absolute_file_path)
    return df


def get_last_pycaret_result_as_dict(df):
    result_dict = df.to_dict(orient="index")
    return result_dict


def get_last_pycaret_result_as_list(df):
    result_dict = df.to_dict(orient="records")
    return result_dict


def get_or_create_task_path(workspace, task_id):
    setup_pickle_path = get_workspace_file_path(workspace.name, f"{task_id}")
    if not os.path.isdir(setup_pickle_path):
        os.makedirs(setup_pickle_path)
    return setup_pickle_path


def save_pull_results(df, task_path, filename="result.csv"):
    full_path = os.path.join(task_path, filename)
    if os.path.exists(full_path):
        # The following works same as the rreplace
        new_filename = str(uuid4())[:8]
        new_filename.join(full_path.rsplit("result", 1))
    if isinstance(df, pd.DataFrame):
        df.to_csv(full_path, index=False)
    else:
        df.data.to_csv(full_path, index=False)
    return full_path


def get_model_plotpath(task_id, workspace, model_filename):
    plot_path = get_or_create_task_path(workspace, task_id)
    # plot_path += f"/{model_filename}.png"
    return plot_path
