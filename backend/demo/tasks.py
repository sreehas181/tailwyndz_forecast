from celery.utils.log import get_task_logger
from demo.classification_tasks import analyze_model as analyze_model_classification
from demo.classification_tasks import create_model_task as create_model_classification
from demo.classification_tasks import deploy_model as deploy_model_classification
from demo.classification_tasks import initialize as initialize_classification
from demo.classification_tasks import predict as predict_classification
from demo.regression_tasks import analyze_model as analyze_model_regression
from demo.regression_tasks import create_model_task as create_model_regression
from demo.regression_tasks import deploy_model as deploy_model_regression
from demo.regression_tasks import initialize as initialize_regression
from demo.regression_tasks import predict as predict_regression
from rest_framework.exceptions import APIException

from backend.celery import app

logger = get_task_logger(__name__)


WORKSPACE_TASKS = {
    "classification": {
        "create_model": create_model_classification,
        "deploy_model": deploy_model_classification,
        "analyze_model": analyze_model_classification,
        "initialize": initialize_classification,
        "predict": predict_classification,
    },
    "regression": {
        "create_model": create_model_regression,
        "deploy_model": deploy_model_regression,
        "analyze_model": analyze_model_regression,
        "initialize": initialize_regression,
        "predict": predict_regression,
    },
}


def clean_workspace_type(workspace_type):
    workspace_type = workspace_type.lower()
    if workspace_type not in WORKSPACE_TASKS:
        raise APIException(
            "workspace_type must be one of: {}".format(list(WORKSPACE_TASKS.keys()))
        )
    if workspace_type in ["timeseries"]:
        raise APIException(
            "workspace_type {} is not supported yet".format(workspace_type)
        )
    return workspace_type


@app.task(bind=True)
def initialize(self, workspace_id=None, data={}, workspace_type="classification"):
    print(workspace_id, data, workspace_type)
    workspace_type = clean_workspace_type(workspace_type)
    task = WORKSPACE_TASKS[workspace_type]["initialize"](workspace_id, data)
    return {
        "request_id": self.request.id,
        "workspace_id": workspace_id,
        "workspace_type": workspace_type,
        "data": data,
        "task": task,
    }


@app.task(bind=True)
def create_model(
    self,
    workspace_id=None,
    parent_task_id=None,
    data={},
    workspace_type="classification",
):
    workspace_type = clean_workspace_type(workspace_type)
    task = WORKSPACE_TASKS[workspace_type]["create_model"](
        workspace_id, parent_task_id, data
    )
    return {
        "request_id": self.request.id,
        "parent_task_id": parent_task_id,
        "workspace_type": workspace_type,
        "workspace_id": workspace_id,
        "data": data,
        "task": task,
    }


@app.task(bind=True)
def analyze_model_task(
    self,
    workspace_id=None,
    parent_task_id=None,
    data={},
    workspace_type="classification",
):
    workspace_type = clean_workspace_type(workspace_type)
    task = WORKSPACE_TASKS[workspace_type]["analyze_model"](
        workspace_id, parent_task_id, data
    )
    return {
        "request_id": self.request.id,
        "parent_task_id": parent_task_id,
        "workspace_type": workspace_type,
        "workspace_id": workspace_id,
        "data": data,
        "task": task,
    }


@app.task(bind=True)
def deploy_model(
    self,
    workspace_id=None,
    parent_task_id=None,
    data={},
    workspace_type="classification",
):
    workspace_type = clean_workspace_type(workspace_type)
    task = WORKSPACE_TASKS[workspace_type]["deploy_model"](
        workspace_id, parent_task_id, data
    )
    return {
        "request_id": self.request.id,
        "parent_task_id": parent_task_id,
        "workspace_id": workspace_id,
        "workspace_type": workspace_type,
        "data": data,
        "task": task,
    }


@app.task(bind=True)
def predict(self, record_id=None, input_data={}, workspace_type="classification"):
    workspace_type = clean_workspace_type(workspace_type)
    task = WORKSPACE_TASKS[workspace_type]["predict"](record_id, input_data)
    return {
        "record_id": record_id,
        "input_data": input_data,
        "workspace_type": workspace_type,
        "task": task,
    }
