import json

from celery.contrib.testing.worker import start_worker
from django.test import Client, RequestFactory, TestCase, TransactionTestCase
from django.urls import reverse
from itsdangerous import Serializer
from rest_framework import status

from .models import Document, Record, Workspace
from .serializers import WorkspaceDetailSerializer, WorkspaceSerializer
from .tasks import analyze_model_task, create_model, deploy_model, initialize, predict


class WorkspaceTests(TestCase):
    def setUp(self):
        self.workspace_1 = Workspace.objects.create(
            name="workspace 1", type="CLASSIFICATION"
        )
        self.workspace_2 = Workspace.objects.create(
            name="workspace 2", type="REGRESSION"
        )
        self.workspace_3 = Workspace.objects.create(
            name="workspace 3", type="TIMESERIES"
        )

    def test_get_all_workspace(self):
        client = Client()
        url = reverse("workspace-list")
        request = RequestFactory().get(url)
        response = client.get(url)
        workspaces = Workspace.objects.all()
        serializer = WorkspaceSerializer(
            workspaces, many=True, context={"request": request}
        )
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_single_workspace(self):
        client = Client()
        workspace_id = int(self.workspace_1.id)
        url = reverse("workspace-detail", args=[workspace_id])
        request = RequestFactory().get(url)
        response = client.get(url)
        workspace = Workspace.objects.get(id=workspace_id)
        serializer = WorkspaceDetailSerializer(workspace, context={"request": request})
        serialized_data = serializer.data
        serialized_data["tasks"] = {}
        self.assertEqual(response.data, serialized_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


# class GetAllPuppiesTest(TestCase):
#     """ Test module for GET all puppies API """

#     def setUp(self):
#         Puppy.objects.create(
#             name='Casper', age=3, breed='Bull Dog', color='Black')
#         Puppy.objects.create(
#             name='Muffin', age=1, breed='Gradane', color='Brown')
#         Puppy.objects.create(
#             name='Rambo', age=2, breed='Labrador', color='Black')
#         Puppy.objects.create(
#             name='Ricky', age=6, breed='Labrador', color='Brown')

#     def test_get_all_puppies(self):
#         # get API response
#         response = client.get(reverse('get_post_puppies'))
#         # get data from db
#         puppies = Puppy.objects.all()
#         serializer = PuppySerializer(puppies, many=True)
#         self.assertEqual(response.data, serializer.data)
#         self.assertEqual(response.status_code, status.HTTP_200_OK)

#     def test_get_invalid_single_puppy(self):
#         response = client.get(
#             reverse('get_delete_update_puppy', kwargs={'pk': 30}))
#         self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

# class CreateNewPuppyTest(TestCase):
#     """ Test module for inserting a new puppy """

#     def setUp(self):
#         self.valid_payload = {
#             'name': 'Muffin',
#             'age': 4,
#             'breed': 'Pamerion',
#             'color': 'White'
#         }
#         self.invalid_payload = {
#             'name': '',
#             'age': 4,
#             'breed': 'Pamerion',
#             'color': 'White'
#         }

#     def test_create_valid_puppy(self):
#         response = client.post(
#             reverse('get_post_puppies'),
#             data=json.dumps(self.valid_payload),
#             content_type='application/json'
#         )
#         self.assertEqual(response.status_code, status.HTTP_201_CREATED)

#     def test_create_invalid_puppy(self):
#         response = client.post(
#             reverse('get_post_puppies'),
#             data=json.dumps(self.invalid_payload),
#             content_type='application/json'
#         )
#         self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

# class FooTaskTestCase(TransactionTestCase):
#     @classmethod
#     def setUpClass(cls):
#         super().setUpClass()
#         cls.celery_worker = start_worker(app)
#         cls.celery_worker.__enter__()
#     @classmethod
#     def tearDownClass(cls):
#         super().tearDownClass()
#         cls.celery_worker.__exit__(None, None, None)
#     def setUp(self):
#         super().setUp()
#         self.task = tasks.foo.delay("bar") # whatever your method and args are
#         self.results = self.task.get()
#     def test_success(self):
#         assert self.task.state == "SUCCESS"
