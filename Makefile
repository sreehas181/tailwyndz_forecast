SHELL := /bin/bash # Use bash syntax
ARG := $(word 2, $(MAKECMDGOALS) )

clean:
	@find . -name "*.pyc" -exec rm -rf {} \;
	@find . -name "__pycache__" -delete

test:
	docker-compose run backend python manage.py test $(ARG) --parallel --keepdb

test-reset:
	python backend/manage.py test backend/ $(ARG) --parallel

format:
	black . --exclude ".+/(settings|migrations)/.+"

docker-test:
	docker-compose run --rm backend python manage.py test $(ARG) --parallel --keepdb

docker-test_reset:
	docker-compose run --rm backend python manage.py test $(ARG) --parallel

docker-up:
	docker-compose up

docker-update_dependencies:
	docker-compose down
	docker-compose up -d --build

docker-down:
	docker-compose down

docker-logs:
	docker-compose logs -f $(ARG)

docker-makemigrations:
	docker-compose run --rm backend python manage.py makemigrations

docker-missing-migrations:
	docker-compose run --rm backend python manage.py makemigrations --check --dry-run

docker-prospector:
	docker-compose run --rm backend pip3 install prospector && prospector --messages-only

docker-migrate:
	docker-compose run --rm backend python manage.py migrate --fake django_celery_results 0006_taskresult_date_created
	docker-compose run --rm backend python manage.py migrate

shell:
	docker-compose run backend python manage.py shell

docker-build:
	docker-compose build $(ARG)

docker-createsu:
	docker-compose run --rm backend python manage.py createsu

copy_env:
	/bin/cp backend/.env.template backend/.env

first_run: copy_env docker-makemigrations docker-migrate docker-createsu

ecr-login:
	aws ecr get-login-password --region $(AWS_DEFAULT_REGION) | docker login --username AWS --password-stdin {account}.dkr.ecr.us-east-1.amazonaws.com

kc-forward:
	bash scripts/forward-ports $(ARG)

kc-deploy:
	kubectl apply -f deployments/redis
	kubectl apply -f deployments/django
	kubectl apply -f deployments/celery
	kubectl apply -f deployments/flower
